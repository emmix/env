#!/bin/sh

wget https://www.golangtc.com/static/go/1.9.2/go1.9.2.linux-amd64.tar.gz 
sudo tar -zxvf  go1.9.2.linux-amd64.tar.gz -C /usr/local/
#sudo add-apt-repository ppa:gophers/archive
#sudo apt-get update
#sudo apt-get install golang-1.9-go

gopath=~/Work/planetchain/go
mkdir -p $gopath

echo "" >> ~/.bashrc
echo "export GOPATH=$gopath" >> ~/.bashrc
echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc
echo "" >> ~/.bashrc
