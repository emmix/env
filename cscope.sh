#!/bin/sh

#set -x

showUsage () {
	echo "$1 [-s \"srcPaths\"] [-o outPath]"
}

srcDirs=""
outDir=""

while getopts "s:o:h" opt
do
	case $opt in
		s)
			srcDirs=$OPTARG
			;;
		o)
			outDir=$OPTARG
			;;
	   ?|h)
			showUsage `basename $0`
			exit 1
	esac
done

if [ -z "$srcDirs" ] ; then
	srcDirs=`pwd`
fi

if [ -z "$outDir" ] ; then
	outDir=`pwd`
fi

srcDirs=`readlink -m $srcDirs | tr '\n' ' '`
for dir in $srcDirs $outDir
do
	if [ ! -d $dir ]
	then
		echo "$dir does not exist!!"
		exit 2
	fi
done

# generate cscope.files
find $srcDirs \( -iname "*.[chxs]" -o -iname "*.hpp" -o -iname "*.cpp" \) -print > $outDir/cscope.files
#ack-grep -f --cpp $SRC_DIR >> cscope.files

cd $outDir
cscope -bkq -i cscope.files
#ctags -R

