#!/bin/bash
set -x

# install packages and tools
sudo apt-get install cscope
sudo cp ./repo /usr/bin/



case "$OSTYPE" in
	darwin*)
		echo "OSX"
		cp  ~/.bash_profile ~/bash_profile.bk
		cp  ./bashrc ~/.bash_profile
		;;
	linux*)
		echo "LINUX"
		cp  ~/.bashrc ~/bashrc.bk
		cat ./bashrc >> ~/.bashrc
		;;
	*)
		echo "unknown: $OSTYPE" ;;
esac

# cscope.sh
sudo cp ./cscope.sh /usr/local/bin/

# ssh
cp ./dotssh/config ~/.ssh/


./installpre.sh
./intallgo.sh
./installvim.sh
./installpost.sh




