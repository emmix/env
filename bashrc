

# bob bash completion
source /usr/local/lib/bob/contrib/bash-completion


export PROMPT_DIRTRIM=1
export GOPATH=$HOME/Project/go
export PATH=$GOPATH/bin:$PATH
export REPO_URL='https://mirrors.tuna.tsinghua.edu.cn/git/git-repo/'

# lauterbach jtag
export T32SYS=/opt/t32
export T32TMP=/tmp
export T32ID=T32
alias 't32=$T32SYS/bin/pc_linux64/t32marm64-qt -c $T32SYS/config.t64 -s $T32SYS/h3menu.cmm'


alias 'll=ls -l'
alias 'cns=cd /home/jfeng/Work/jpcc/cns.integration'
alias 'mib=cd /home/jfeng/Work/jpcc/mib3.integration'
alias 'doc=cd /home/jfeng/Work/jpcc/svw-cns3.0/01_ProjectManagement/08_ResourceManagement/03_DevEnvSetup/SystemDepartment'


