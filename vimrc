

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

set shiftwidth=3
set tabstop=3

call plug#begin()
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'https://github.com/kien/ctrlp.vim'
Plug 'diabloneo/cscope_maps.vim'
Plug 'vim-scripts/autoload_cscope.vim'
Plug 'vim-scripts/a.vim'
Plug 'othree/xml.vim'
Plug 'elzr/vim-json'
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
Plug 'moll/vim-node'
call plug#end()

" nerdtree
map <C-n> :NERDTreeToggle<CR>

" xml.vim
" fold: \f
" unfold: l
" refer to: help xml-plugin-mappings 

" ctrlp.vim
" for keymap, refer to: help ctrlp

" vim-go
" :help vim-go
let g:go_version_warning = 0

" tern
" help tern
let g:tern_map_keys = 1


execute pathogen#infect()
call pathogen#helptags()

set pastetoggle=<F4>

