#!/bin/bash

# install vim 8.0
sudo apt remove vim
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt-get install vim

# install apt-vim
curl -sL https://raw.githubusercontent.com/egalpin/apt-vim/master/install.sh | sh

#
# plugins for js
#
apt-vim install -y https://github.com/Valloric/YouCompleteMe.git
pushd .
cd ~/.vim/bundle/YouCompleteMe
git submodule update --init --recursive
sudo apt-get remove ninja  # fix some cmake errors
./install.py --js-completer
popd
apt-vim install -y https://github.com/ternjs/tern_for_vim.git
pushd .
cd ~/.vim/bundle/tern_for_vim
npm install
popd



cp  ~/.vimrc ~/vimrc.bk
cp  ./vimrc ~/.vimrc

#curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
#git clone https://github.com/fatih/vim-go.git ~/.vim/plugged/vim-go


# patch autoload_cscope vim plugin
curdir=`pwd`
pushd .
cd ~/.vim/plugged/autoload_cscope.vim
git am ${curdir}/0001-autoload-cscope-for-.hpp-and-.cpp.patch
popd

